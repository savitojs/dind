# Ansible Role: Chrony

Installs and configures chrony.

## Requirements

None.

## Role Variables

### `defaults/main.yml`

* `chrony_packages:`
    ```
      - chrony
    ```

* `chrony_service_name: chronyd`

* `chrony_config_location: /etc/chrony.conf`

* `chrony_config_server:`
    ```
      - 0.rhel.pool.ntp.org
    ```

* `chrony_config_allow: []`

* `chrony_config_logdir: '/var/log/chrony'`

* `ntp_services` - list of ntp services to stop


## Dependencies

None.

## Example Playbook

1) Install chrony and use the default settings.

```
	- hosts: all
	  roles:
	  - chrony
```

2) Install chrony and use custom servers.

```
	- hosts: all
	  roles:
	  - role: chrony
          chrony_config_server:
          - 0.rhel.pool.ntp.org
          chrony_config_allow:
          - 192.168.1/24
```
